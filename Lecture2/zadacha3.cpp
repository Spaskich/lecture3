#include <iostream>
#include <string>
using namespace std;
int main(){
    string firstname, lastname, address, date;
    cout<<"First name: ";
    cin>>firstname;
    cout<<"Last name: ";
    cin>>lastname;
    cout<<"Address: ";
    cin.get();
    getline(cin, address);
    cout<<"Date of birth: ";
    cin>>date;
    cout<<endl<<endl;
    cout<<"*********************************************************\n";
    cout<<"> "<<firstname<<endl;
    cout<<"> "<<lastname<<endl;
    cout<<"> "<<address<<endl;
    cout<<"> "<<date<<endl;
    cout<<"*********************************************************\n";
    return 0;
}
