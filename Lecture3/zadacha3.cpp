#include <iostream>
#include <cmath>
using namespace std;

int main(){
    int a;
    cin >> a;
    const float km= 0.001;
    const float dm = 10;
    const float cm = 100;
    cout << a*km << endl;
    cout << a*dm << endl;
    cout << a*cm << endl;
    return 0;
}
