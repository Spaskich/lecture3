#include <iostream>
#include <cmath>
using namespace std;

int main(){
    float price, amount;
    cout << "Input price: ";
    cin >> price;
    cout << "Input amount: ";
    cin >> amount;
    cout << "Change in leva: " << amount-price << " lv" << endl;
    cout << "Change in stotinki: " << (amount-price)*100 << " st" << endl;
    return 0;
}
